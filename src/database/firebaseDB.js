import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAxDPF28-a9zZ0v_4rXhz8mDCqZKlYYRTA",
    authDomain: "sanberapp-bfada.firebaseapp.com",
    projectId: "sanberapp-bfada",
    storageBucket: "sanberapp-bfada.appspot.com",
    messagingSenderId: "193517219018",
    appId: "1:193517219018:web:0fe96b57ba45c4c9e09833",
    measurementId: "G-HBNT5XXZJT"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;