import { createStore, applyMiddleware } from 'redux'
// import rootReducer from './reducers'
import {reducer} from './reducers'

// Mendefinisikan store menggunakan reducer
const store = createStore(reducer)

export default store