// Mendefinisikan types yang merupakan action yang akan digunakan untuk 
// memodifikasi state pada store
export const types = {
    SET_MOVIE_LIST: 'SET_MOVIE_LIST',
    SET_DETAIL_MOVIE: 'SET_DETAIL_MOVIE',
    SET_FAVORIT_MOVIE_LIST: 'SET_FAVORIT_MOVIE_LIST',
    REMOVE_FAVORIT_MOVIE_LIST: 'REMOVE_FAVORIT_MOVIE_LIST',
}

// Fungsi tambahan untuk men-dispatch action, dengan tambahan data payload
export const actionCreators = {
    setMovieList: item => {
        return { type: types.SET_MOVIE_LIST, payload: item }
    },
    setDetailMovie: item => {
        return { type: types.SET_DETAIL_MOVIE, payload: item }
    },
    setFavoritMovieList: item => {
        return { type: types.SET_FAVORIT_MOVIE_LIST, payload: item }
    },
    removeFavoritMovieList: item => {
        return { type: types.REMOVE_FAVORIT_MOVIE_LIST, payload: item }
    },
}

// Initial state dari store
const initialState = {
    movies: [],
    detailMovie: null,
    favoritMovies: [],
}

// Fungsi untuk mengatur action dan update state yang terdapat pada store.
// Catatan:
// - reducer harus me-return state baru dalam bentuk object. Tidak boleh 
//   secara langsung mengubah (assign) state secara langsung. State harus 
//   diperlakukan sebagai immutable.
// - Pada reducer ditambahkan default value dari state = initialState.
//   Redux akan memanggil reducer() tanpa state di awal, sehingga pada 
//   aplikasi ini state awal sudah terisi dengan initialState.
export const reducer = (state = initialState, action) => {
    const { favoritMovies } = state
    const { type, payload } = action

    switch (type) {
        case types.SET_MOVIE_LIST: {
            return {
                ...state,
                movies: payload,
            }
        }
        case types.SET_DETAIL_MOVIE: {
            return {
                ...state,
                detailMovie: payload,
            }
        }
        case types.SET_FAVORIT_MOVIE_LIST: {
            return {
                ...state,
                favoritMovies: [payload, ...favoritMovies],
            }
        }
        case types.REMOVE_FAVORIT_MOVIE_LIST: {
            return {
                ...state,
                favoritMovies: favoritMovies.filter((item, i) => item.id !== payload.id),
            }
        }
    }

    return state
}