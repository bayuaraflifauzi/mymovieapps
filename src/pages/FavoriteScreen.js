import React from 'react'
import { StyleSheet, TouchableOpacity, SafeAreaView, FlatList, Image, View, Text, TextInput } from 'react-native'
import { connect } from 'react-redux'
import { actionCreators } from '../store/reducers'
import firebase from '../database/firebaseDB'

class Favorite extends React.Component {
    state = { loggedIn: null };

    constructor(props) {
        super(props)
        this.state = { 
            uid: ''
        }
    }
    
    showMovieDetail(data) {
        const { navigation } = this.props;
        const { dispatch } = this.props

        dispatch(actionCreators.setDetailMovie(data)) 
        navigation.navigate('Detail Screen')
    }

    render() {
        this.state = { 
            email: firebase.auth().currentUser.email,
            uid: firebase.auth().currentUser.uid
        }  
        const { favoritMovies } = this.props
        return (
            <View style={styles.container}>
                <Image
                    style={styles.logo}
                    source={ require('./assets/Logo.png') }
                />

                <Text style={{color: 'white', fontSize: 24, fontWeight: 'bold'}}>{this.state.email}</Text>
                <Text style={{color: 'white', marginBottom: 30}}>{this.state.uid}</Text>

                <View style={styles.containerForm}>
                    <View style={styles.formBox}>
                        <Text style={styles.title}>My Favorite</Text>

                        <SafeAreaView>
                            <FlatList
                                numColumns={2}
                                data={favoritMovies}
                                keyExtractor={(item) => item.id}
                                renderItem={({item}) => {
                                    return(
                                        <TouchableOpacity onPress={this.showMovieDetail.bind(this, item)}>
                                            <View style={styles.box}>
                                                <View style={styles.headerBox}>
                                                    <Image
                                                        style={[styles.image, {borderRadius: 5}]}
                                                        source={{
                                                            uri: 'https://image.tmdb.org/t/p/w500' + item.poster_path,
                                                        }}
                                                    />
                                                </View>
                                                <View style={styles.bodyBox}>
                                                    <Text style={{flex: 1, flexWrap: 'wrap', fontWeight: 'bold'}}>{item.title}</Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>   
                                    )
                                }}
                            />
                        </SafeAreaView>
                                    

                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#00B4D8',
        alignItems: 'center'
    },
    containerForm:{
        flex: 3,    
        backgroundColor: 'white',
        width: 360,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
    },
    logo:{
        marginTop: 30,
        marginBottom: 10,
        width: 154,
        height: 91
    },
    image:{
        width: 120,
        height: 160,
        borderWidth: 1,
        borderColor: 'black'
    },
    formBox:{
        marginTop: 30,
        marginHorizontal: 30,
        alignItems: 'center'
    },
    title:{
        fontSize: 36
    },
    input:{
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        borderRadius: 5,
        width: 299,
        height: 40,
        marginTop: 5,
        marginBottom: 10,
        borderBottomColor: '#C4C4C4'
    },
    button:{
        alignItems: "center",
        justifyContent: 'center',
        width: 299,
        height: 40,
        borderRadius: 8,
        marginBottom: 10
    },
    textButton:{
        color: 'white'
    }, 
    box:{
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: 10,
        marginHorizontal: 5
    },
    headerBox:{
        paddingHorizontal: 10
    },
    bodyBox:{
        marginVertical: 5,
        flexDirection: 'row',
        marginHorizontal: 10
    }
})

const mapStateToProps = (state) => ({
    favoritMovies: state.favoritMovies,
})

export default connect(mapStateToProps)(Favorite)