import React from 'react'
import { StyleSheet, Image, View } from 'react-native'

export default function Splash() {
    return (
        <View style={styles.container}>
            <Image
                style={styles.logo}
                source={ require('./assets/Logo.png') }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#00B4D8',
        alignItems: 'center',
        justifyContent: 'center'
    }
})