import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Image, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import firebase from '../database/firebaseDB';

export default class Register extends Component {
    constructor(){
        super()
        this.state = {
            name: '',
            email: '',
            phone: '',
            password: '',
            isLoading: false
        }
    }

    updateInputVal = (val, prop) => {
        const state = this.state;
        state[prop] = val;
        this.setState(state);
    }

    registerUser = () => {
        if(this.state.email === '' && this.state.password === '') {
            Alert.alert('Enter details to signup!')
        } else {
            this.setState({
                isLoading: true,
            })

            firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((res) => {
            res.user.updateProfile({
                name: this.state.name
            })
            Alert.alert('User registered successfully!')
            console.log('User registered successfully!')
            this.setState({
                isLoading: false,
                name: '',
                phone: '',
                email: '', 
                password: ''
            })
            this.props.navigation.replace('Login Screen')
            })
            .catch((err) => {
                Alert.alert("Error","Register gagal! silahkan lakukan kembali")
                this.props.navigation.replace('Register Screen')
            })      
        }
    }

    render(){
        if(this.state.isLoading){
            return(
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#9E9E9E"/>
                </View>
            )
        }
        
        return (
            <View style={styles.container}>
                <Image
                    style={styles.logo}
                    source={ require('./assets/Logo.png') }
                />

                <View style={styles.containerForm}>
                    <View style={styles.formBox}>
                        <Text style={styles.title}>Sign Up</Text>

                        <TextInput 
                            style={styles.input}
                            placeholder= "Name"
                            value= {this.state.name}
                            onChangeText= {(value)=>this.updateInputVal(value, 'name')}
                        />

                        <TextInput 
                            style={styles.input}
                            placeholder= "Email"
                            value= {this.state.email}
                            onChangeText= {(value)=>this.updateInputVal(value, 'email')}
                        />

                        <TextInput 
                            style={styles.input}
                            placeholder= "Phone Number"
                            value= {this.state.phone}
                            onChangeText= {(value)=>this.updateInputVal(value, 'phone')}
                        />

                        <TextInput 
                            style={styles.input}
                            placeholder= "Password"
                            value= {this.state.password}
                            onChangeText= {(value)=>this.updateInputVal(value, 'password')}
                        />

                        <TouchableOpacity
                            style={[styles.button, {backgroundColor: "#023E8A"}]}
                            onPress={() => this.registerUser()}
                        >
                            <Text style={styles.textButton}>Register</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.button, {backgroundColor: "white", borderColor: '#FB8500', borderWidth: 1}]}    
                            onPress={() => this.props.navigation.replace('Login Screen')}
                        >
                            <Text style={{color: 'black'}}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#00B4D8',
        alignItems: 'center'
    },
    containerForm:{
        flex: 3,    
        backgroundColor: 'white',
        width: 360,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
    },
    logo:{
        marginVertical: 50,
    },
    formBox:{
        marginTop: 30,
        marginHorizontal: 30
    },
    title:{
        fontSize: 36
    },
    input:{
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        borderRadius: 5,
        width: 299,
        height: 40,
        marginTop: 5,
        marginBottom: 10,
        borderBottomColor: '#C4C4C4'
    },
    button:{
        alignItems: "center",
        justifyContent: 'center',
        width: 299,
        height: 40,
        borderRadius: 8,
        marginBottom: 10
    },
    textButton:{
        color: 'white'
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    }
});