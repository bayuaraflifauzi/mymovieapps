import React from 'react'
import { StyleSheet, TouchableOpacity, ScrollView, SafeAreaView, Image, View, Text, Alert } from 'react-native'
import { connect } from 'react-redux'
import { actionCreators } from '../store/reducers'
import { Data }from './data'

class Detail extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isExist: false
        }
    }

    componentDidMount() {
        this.checkIfExist();
    }

    checkIfExist(){
        const { favoritMovies, detailMovie } = this.props
        
        const moviesFav = favoritMovies.some(function(el) {
            return el.id === detailMovie.id;
        });

        console.log('checkIfExist : ', moviesFav)
        this.setState({
            isExist: moviesFav
        })
    }

    addToFavorite() {
        const { dispatch, detailMovie } = this.props
        dispatch(actionCreators.setFavoritMovieList(detailMovie))
        Alert.alert("Add to favorite, sucsess!")
        setTimeout(() => {
            this.checkIfExist() 
        }, 300);
    }

    removeFromFavorite() {
        const { dispatch, detailMovie } = this.props
        dispatch(actionCreators.removeFavoritMovieList(detailMovie))
        Alert.alert("Remove film from favorite, sucsess!")
        setTimeout(() => {
            this.checkIfExist() 
        }, 300);
    }
    

    render() {
        const { detailMovie } = this.props
        return (
            <View style={styles.container}>
                <Image
                    style={styles.logo}
                    source={ require('./assets/Logo.png') }
                />

                <Text style={{color: 'white', fontSize: 24, fontWeight: 'bold'}}>Bayu Arafli Fauzi</Text>
                <Text style={{color: 'white', marginBottom: 30}}>bayuaraflifauzi@gmail.com</Text>

                <View style={styles.containerForm}>
                    <SafeAreaView>
                        <ScrollView>
                            <View style={styles.formBox}>
                                <Text style={[styles.title, {fontWeight: 'bold'}]}>{detailMovie.title}</Text>
                                
                                <Image
                                    style={styles.image}
                                    source={{
                                        uri: 'https://image.tmdb.org/t/p/w500' + detailMovie.poster_path,
                                    }}
                                />
                                <View style={{flexDirection: 'row', width: '100%', paddingHorizontal: 30, marginTop: 10}}>
                                    <Text style={{fontSize: 24, fontWeight: 'bold'}}>Rating : </Text>
                                    <Text style={{fontSize: 24}}>{detailMovie.vote_average}</Text>
                                </View>

                                <View style={{width: '100%', paddingHorizontal: 30, marginTop: 10, marginBottom: 15}}>
                                    <Text style={{fontSize: 24, fontWeight: 'bold'}}>Sinopsis : </Text>
                                    <Text style={{fontSize: 18}}>{detailMovie.overview}</Text>
                                </View>   


                                {this.state.isExist ? (
                                    <TouchableOpacity
                                        style={[styles.button, {backgroundColor: "#D23130"}]}
                                        onPress={this.removeFromFavorite.bind(this)}
                                    >
                                        <Text style={styles.textButton}>Remove from favorite</Text>
                                    </TouchableOpacity>
                                ) : (
                                    <TouchableOpacity
                                        style={[styles.button, {backgroundColor: "#023E8A"}]}
                                        onPress={this.addToFavorite.bind(this)}
                                    >
                                        <Text style={styles.textButton}>Add to favorite</Text>
                                    </TouchableOpacity>
                                )}
                                

                            </View>
                        </ScrollView>
                    </SafeAreaView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#00B4D8',
        alignItems: 'center'
    },
    containerForm:{
        flex: 3,    
        backgroundColor: 'white',
        width: 360,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
    },
    logo:{
        marginTop: 30,
        marginBottom: 10,
        width: 154,
        height: 91
    },
    image:{
        width: 150,
        height: 200,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'black'
    },
    formBox:{
        marginVertical: 30,
        alignItems: 'center'
    },
    title:{
        fontSize: 28,
        flexWrap: 'wrap',
        marginHorizontal: 15
    },
    input:{
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        borderRadius: 5,
        width: 299,
        height: 40,
        marginTop: 5,
        marginBottom: 10,
        borderBottomColor: '#C4C4C4'
    },
    button:{
        alignItems: "center",
        justifyContent: 'center',
        width: 299,
        height: 40,
        borderRadius: 8,
        marginBottom: 10
    },
    textButton:{
        color: 'white'
    }, 
    box:{
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: 10,
        marginHorizontal: 5
    },
    headerBox:{
        paddingHorizontal: 10
    },
    bodyBox:{
        marginVertical: 10
    }
})

const mapStateToProps = (state) => ({
    detailMovie: state.detailMovie,
    favoritMovies: state.favoritMovies,
})

export default connect(mapStateToProps)(Detail)