import React from 'react'
import { StyleSheet, Text, Button, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'
import { Ionicons } from '@expo/vector-icons';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

import Splash from '../pages/SplashScreen'
import Login from '../pages/LoginScreen'
import Register from '../pages/RegisterScreen'
import Home from '../pages/HomeScreen'
import Favorite from '../pages/FavoriteScreen'
import Detail from '../pages/DetailScreen'

import Test from '../pages/TestScreen'

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='Login Screen' component={Login} />
                <Stack.Screen name='Register Screen' component={Register} />
                <Stack.Screen name='Main Screen' component={MainApp} />
                <Stack.Screen name='Detail Screen' component={Detail} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused
                ? 'ios-home'
                : 'ios-home';
            } else if (route.name === 'Favorite') {
              iconName = focused ? 'star' : 'star';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
    >

        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Favorite" component={Favorite} />
    </Tab.Navigator>
    
)