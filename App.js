import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import Apps from './src/index'

// Import store
import store from './src/store/index'

export default function App() {
  return (
    <Provider store={store}>
      <Apps />
    </Provider>
  );
}